'''
@file   FSM_Elevator.py

This file is a finite state machine that simulates the behavior of an elevtaor.

The user has 2 buttons to move the elevator to either floor 1 or floor 2.

There are also proximity sensors at each floor inidcating which floor has been reached.

@author   John Ringrose
@image html IMG_0447.jpg
@image html IMG_0448.jpg
'''

from random import choice
import time


class TaskElevator:
    '''
    @brief      A finite state machine to control Elevators.
    @details    This class implements a finite state machine to control the
                operation of Elevators.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                  = 0
    
    ## Constant defining State 1 - Moving Down
    S1_MOVING_DOWN           = 1
    
    ## Constant defining State 2 - Moving Up
    S2_MOVING_UP             = 2
    
    ## Constant defining State 3 - Stopped on Floor 1
    S3_STOPPED_ON_FLOOR_1    = 3
    
    ## Constant defining State 4 - Stopped on Floor 2
    S4_STOPPED_ON_FLOOR_2    = 4
    
    
    def __init__(self, interval, Motor, First, Second, Button_1, Button_2):
        '''
        @brief      Creates a TaskWindshield object.
        '''
        
          ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## The button object used for the First Floor
        self.First = First
        
        ## The button object used for the Second Floor
        self.Second = Second
        
        ## The button object used for the First Floor button
        self.Button_1= Button_1
        
        ## The button object used for the Second Floor button
        self.Button_2= Button_2
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = time.time() # The number of seconds since Jan 1. 1970
    
        ## The interval of time, in seconds, between runs of the task
        self.interval = interval         
    
        ## The "timestamp" for when to run the task next
        self.next_time = self.start_time + self.interval
    
    def run(self):
        '''
        @brief      Runs one iteration of the Elevatortask.
        '''
        
        self.curr_time = time.time()    
        '''
        @brief      Updates the current timestamp.
        '''
        
        if (self.curr_time >= self.next_time):
            if(self.state == self.S0_INIT):
                print(str(self.runs) + ' State 0 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 0 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Downward()
            
            elif(self.state == self.S1_MOVING_DOWN):
                print(str(self.runs) + ' State 1 {:0.2f}'.format(self.curr_time - self.start_time)) 
                # Run State 1 Code
                if self.First.getButtonFloor():
                    self.transitionTo(self.S3_STOPPED_ON_FLOOR_1)
                    self.Motor.Hold()
                    self.Button_1.getButtonstate(False)
                    self.Button_2.getButtonstate(False)
        
            elif(self.state == self.S3_STOPPED_ON_FLOOR_1):
                print(str(self.runs) + ' State 3 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 2 Code
                self.transitionTo(self.S2_MOVING_UP)
                self.Button_2.getButtonstate(True)
                self.Motor.Upward()
               
            elif(self.state == self.S2_MOVING_UP):
                print(str(self.runs) + ' State 2 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 3 Code
                if self.Second.getButtonFloor():
                    self.transitionTo(self.S4_STOPPED_ON_FLOOR_2)
                    self.Motor.Hold()
                    self.Button_1.getButtonstate(False)
                    self.Button_2.getButtonstate(False)
                    
            elif(self.state == self.S4_STOPPED_ON_FLOOR_2):
                print(str(self.runs) + ' State 4 {:0.2f}'.format(self.curr_time - self.start_time))
                # Run State 4 Code
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Button_1.getButtonstate(True)
                self.Motor.Downward()
                
            elif(self.state == self.S5_DO_NOTHING):
                print(str(self.runs) + ' State 5 {:0.2f}'.format(self.curr_time - self.start_time))
                
            else:
                # Uh-oh state (undefined sate)
                # Error handling
                pass
            
            self.runs += 1
            self.next_time += self.interval # updating the "Scheduled" timestamp
    
            
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run.
        '''
        self.state = newState
        


class Button:
    '''
    @brief      A button class
    @details    This class represents a button that the can be pushed by the
                imaginary user to select the desired floor.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonstate(self,state):
        '''
        @brief      This retrieves the button state.
        @details    This must be selected true or false to indicate which floor
                    button the user chose to press.
        @return     A boolean representing the state of the button.
        '''
        return state
    
    def getButtonFloor(self):
        '''
        @brief      This returns a randomized floor state until the floor has been reached.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])

class Motor:
    '''
    @brief      A cable-driven motor.
    @details    This class represents an elevator motor used to make the eleavtor move
                upwards, downwards, and stopped.
    '''
    
    def __init__(self):
        '''
        @brief Creates a Motor Object
        '''
        pass
    
    def Upward(self):
        '''
        @brief Moves the elevator upward
        '''
        print('Elevator moving up')
    
    def Downward(self):
        '''
        @brief Moves the elevator downward
        '''
        print('Elevator moving Downward')
    
    def Hold(self):
        '''
        @brief Holds the elevator stationary
        '''
        print('Elevator Stopped')
        


