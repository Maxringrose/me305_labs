## @file motor.py
#  Brief doc for motor.py
#
#  Detailed doc for motor.py 
#
#  @author John Ringrose
#
#  @copyright License Info
#
#  @date September 15, 2020
#
#  @package motor
#  Brief doc for the motor module
#
#  Detailed doc for the motor module
#
#  @author John Ringrose
#
#  @copyright License Info
#
#  @date September 15, 2020

import pyb

## A motor driver object
#
#  Details
#  @author John Ringrose
#  @copyright License Info
#  @date September 15, 2020
class Motor:

	## Constructor for motor driver
	#
	#  Detailed info on motor driver constructor
	def __init__(self):
		pass

	## Sets duty cycle for motor
	#
	#  Detailed info on motor driver duty cycle function
	#
	#  @param level The desired duty cycle
	def set_duty_cycle(self,level):
		pass