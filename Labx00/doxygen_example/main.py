## @file main.py
#  Brief doc for main.py
#
#  Detailed doc for main.py 
#
#  @author John Ringrose
#
#  @copyright License Info
#
#  @date September 15, 2020
#
#

import motor

## A motor driver object
moto = motor.MotorDriver()

moto.set_duty_cycle(50)