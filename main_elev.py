# -*- coding: utf-8 -*-
'''
@file main_elev.py
'''


from FSM_Elevator import Button, Motor, TaskElevator
        
## Motor Object
Motor = Motor()

## Button Object for "go button"
Button_1 = Button('Pb1')

## Button Object for "go button"
Button_2 = Button('Pb2')

## Button Object for "First Floor"
First = Button('Pb3')

## Button Object for "Second Floor"
Second = Button('Pb4')

## Task object
task1 = TaskElevator(0.1, Motor, First, Second, Button_1, Button_2) # Will also run constructor

# To run the task call task1.run() over and over
for N in range(10000000): #Will change to   "while True:" once we're on hardware
    task1.run()
#    task2.run()
#    task3.run()
