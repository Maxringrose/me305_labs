'''
@file      Fibonaccicalc.py
@brief     A file containing a function that will return a fibonacci number given its corresponding index
@details   This function begins with a cache that will serve as a dictionary. 
           Then the memoization function is defined. Inputs are checked to see if they are in the dictionary key. If so, values are returned toe the corresponding input.
@author    John Ringrose
'''



fibonacci_cache = {}
'''
    @brief   A logging dictionary 
    @details A cache that will serve as a dictionary that rembers balues upon calculating them
'''

input_value = input('Hit enter. Then call out the function fibonacci_num(index) with an index>= 0 . This will give you the corresponding fibonacci number')
'''
    @brief   The input for the user
    @details A prompt for the user to input an index for the calculator 

'''
def fibonacci_num(input_value):
    '''
    @brief A memoization function

    '''
    if input_value in fibonacci_cache:
        return fibonacci_cache[input_value]
    if input_value < 0:         #The next 4 if statements will determine what the fibonacci number is if the number is not cached
            value = ('This is an invalid index, please enter an integer >= 0')
    elif input_value == 0:    
            value = 0
    elif input_value == 1:    
            value = 1        
    elif input_value == 2:
            value = 1
    elif input_value > 2:           
            value =  fibonacci_num(input_value -1) + fibonacci_num(input_value -2)
    fibonacci_cache[input_value] = value
    return(value)


